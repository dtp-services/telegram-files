const $express = require('express');
const $config = require('config');

const app = $express();
const SERVER_PORT = process.env.PORT || $config.get('server.port');

require('./cors')(app);

app.get('/photo/:file_id', require('./photos'));
app.get('/content/*', require('./cache-proxy'));

app.listen(SERVER_PORT, () => {
  console.log('Telegram Files listing :%s port', SERVER_PORT);
});