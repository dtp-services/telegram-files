const $Memcached = require('memcached');
const $Promise = require('bluebird');

const memcached = new $Memcached('127.0.0.1:11211');
const expiredCache = 60 * 55;

class Memcached {
  static keyName (fileId) {
    return `TGUSRPH${fileId}`;
  }

  static async get (fileId) {
    const key = Memcached.keyName(fileId);

    return await new $Promise((resolve, reject) => {
      memcached.get(key, function (err, data) {
        if (err) {
          return resolve();
        }

        resolve(data);
      });
    });
  }

  static async set (fileId, fileURL) {
    const key = Memcached.keyName(fileId);

    return await new $Promise((resolve, reject) => {
      memcached.set(key, fileURL, expiredCache, function (err) {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  }
}

module.exports = Memcached;