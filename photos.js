const $Telegram = require('telegraf/telegram');
const $request = require('request');
const $config = require('config');

const telegram = new $Telegram($config.get('bot.token'));

module.exports = function (req, res) {
  (async () => {
    const fileId = req.params.file_id;
    const link = await telegram.getFileLink(fileId);

    res.writeHead(200, {
      'Content-Type': 'image/jpeg',
      'Cache-Control': 'max-age=2678400000'
    });

    $request
      .get(link)
      .pipe(res);
  })()
    .catch((err) => {
      console.log(err);

      res.end('Error!');
    });

};